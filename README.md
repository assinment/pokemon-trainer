# PokemonTrainer

An webapplication developed with angular and uses [PokeAPi](https://pokeapi.co/) to fetch the data.

Users can create an account with a valid username, and collect pokemons which are saved for the user
or remove specific pokemons.

## How to use

Clone or download the repo and navigate with the terminal to the folder.
Run `ng serve` for a live server if you have Angular installed. To be able to run this 
application you also need valid API-keys.




