import {Pokemon, PokemonResult} from "./pokemon.model";
import {PokemonService} from "../services/pokemon.service";

export interface Trainer {
  id: number;
  username: string;
  pokemon: PokemonResult[];
}
