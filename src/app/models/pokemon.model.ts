
// This model should be changed to match the api.

export interface PokemonResult {
  name: string;
  url: string;
}

export interface Pokemon {
  count: number;
  next: string;
  previous: string;
  results: PokemonResult[];

}
