import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginService} from "../../services/login.service";
import {NgForm} from "@angular/forms";
import {Trainer} from "../../models/trainer.model";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();
  falseButton: boolean = true;

  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService

  ) { }

  public loginSubmit(loginForm: NgForm): void {

    const {username} = loginForm.value;

    this.loginService.login(username)
      .subscribe(
        {
          next: (trainer: Trainer) => {
            this.userService.user = trainer;
            this.login.emit();
          },
          error: () => {
            console.log("Something went wrong in login-form")
          }
        })
  }


}
