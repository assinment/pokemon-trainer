import {Component, Input, OnInit} from '@angular/core';
import {Pokemon, PokemonResult} from "../../models/pokemon.model";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
})

/*
Used for the catalog page and shows the entire list of pokemons fetched from api
If user has collected the pokemon it is shown to the user visually.
*/

export class PokemonListComponent implements OnInit {

  @Input() pokemons: PokemonResult[] = [];

  constructor( readonly userService: UserService) { }

  ngOnInit(): void {
  }

}
