import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Trainer} from "../../models/trainer.model";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {


  // Only shows the navbar if the user is logged in
  get user(): Trainer | undefined {
  return this.userService.user;

  }

  constructor(
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
  }

}
