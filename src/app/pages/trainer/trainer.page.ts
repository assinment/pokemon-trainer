import { Component, OnInit } from '@angular/core';
import {PokemonResult} from "../../models/pokemon.model";
import {PokemonService} from "../../services/pokemon.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
})

// The trainer page is similar to pokemon-list-component. It only
// shows the pokemons that the user has collected and a remove button.
// The remove button is used to remove a pokemon by calling remove.service

export class TrainerPage implements OnInit {

  public get pokemons(): PokemonResult[] {
    return this.pokemonService.pokemon
  }

  public get error(): string {
    return this.pokemonService.error
  }

  constructor(
    private readonly pokemonService: PokemonService,
     readonly userService: UserService
  ) { }

  ngOnInit(): void {
    this.pokemonService.findAllPokemons();
  }

}
