import {Component, Input, OnInit} from '@angular/core';
import {PokemonService} from "../../services/pokemon.service";
import {Pokemon, PokemonResult} from "../../models/pokemon.model";

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',

})
export class CataloguePage implements OnInit {

  public get pokemons(): PokemonResult[] {
    return this.pokemonService.pokemon
  }

  public get error(): string {
    return this.pokemonService.error
  }

  constructor(
    private readonly pokemonService: PokemonService
  ) { }

  // Fetches all pokemons from the api

  ngOnInit(): void {
    this.pokemonService.findAllPokemons();
  }
}
