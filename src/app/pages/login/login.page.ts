import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {PokemonResult} from "../../models/pokemon.model";
import {Trainer} from "../../models/trainer.model";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',

})
export class LoginPage implements OnInit {

  constructor(
    private readonly router: Router,
    readonly userService: UserService
  ) { }

  handleLogin() : void {
    this.router.navigateByUrl("/catalogue");
  }

  ngOnInit(): void {
    if (this.userService.user) {
      this.router.navigate(["/trainer"]);
    }
    else {
      this.router.navigate(["/login"]);
    }
  }
}

