import {Component, Input, OnInit} from '@angular/core';
import {FavoriteService} from "../../services/favorite.service";

@Component({
  selector: 'app-favorite-button',
  templateUrl: './favorite-button.component.html',
})
export class FavoriteButtonComponent implements OnInit {

  @Input() pokemonName: string = "";

  constructor(
    private readonly favoriteService: FavoriteService
  ) { }

  ngOnInit(): void {
  }

  // This method is called when the user clicks the favorite button and lets
  // favoriteService handle the logic of updating the users collected pokemons
  onCollectClick(): void {
    this.favoriteService.collectPokemon(this.pokemonName);

  }
}
