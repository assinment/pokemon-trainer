import {Component, Input, OnInit} from "@angular/core";
import {RemoveService} from "../../services/remove.service";

@Component({
  selector: 'app-remove-button',
  templateUrl: './remove-button.component.html',
})

export class RemoveButtonComponent implements OnInit{

  @Input() pokemonName: string = "";

  constructor(
    private readonly removeService: RemoveService
  ) {}

  // Remove the pokemon from the user's collection, calls removeService.
  onRemoveClick(): void {
    this.removeService.removePokemon(this.pokemonName);
  }

  ngOnInit(): void {
  }




}
