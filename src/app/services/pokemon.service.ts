import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Pokemon, PokemonResult} from "../models/pokemon.model";
import {StorageUtil} from "../utils/storage.util";

const pokeApi = "https://pokeapi.co/api/v2/pokemon/?limit=1000"

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private _pokemonList: PokemonResult [] = [];
  private _error: string = "";

  get pokemon(): PokemonResult [] {
    return this._pokemonList
  }

  get error(): string {
    return this._error
  }

  constructor(private readonly http: HttpClient) {}

  // Fetches pokemons from the api. The limit is set to 1000 in the url.
  // Saved locally.
  public findAllPokemons(): void {
    const storedValue = StorageUtil.storageReadPokemon("pokemonList");
    if (storedValue !== undefined) {
      console.log("Retrieved from storage");
      this._pokemonList = storedValue;
    } else {
      this.http.get<Pokemon>(pokeApi)
        .subscribe({
          next: (pokemon: Pokemon) => {
            this._pokemonList = pokemon.results;

            this._pokemonList.forEach(pokemon => {
              pokemon.url = pokemon.url.replace("https://pokeapi.co/api/v2/pokemon/"
                , "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/");
              pokemon.url = pokemon.url.replace(/.$/, ".png");
            })
            console.log("Retrieved from API");
            StorageUtil.storageSavePokemon("pokemonList", this._pokemonList);
          }
        })
    }
  }


}

