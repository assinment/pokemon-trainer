import { Injectable } from '@angular/core';
import {PokemonService} from "./pokemon.service";
import {UserService} from "./user.service";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {PokemonResult} from "../models/pokemon.model";
import {finalize, Observable, Subscription, tap} from "rxjs";
import {Trainer} from "../models/trainer.model";


const url = environment.apiTrainer;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonService,
    private readonly userService: UserService
  ) { }

  // Saves collected pokemon in the server and updates the user with the new pokemon.
  public collectPokemon(pokemonName: string): Subscription {
    if (!this.userService.user) {
      throw new Error("favorite.service.ts: User does not exist.");
    }
    const userId: number = this.userService.user.id;
    const pokemon: PokemonResult | undefined = this.pokemonService.pokemon.find(p => p.name === pokemonName);

    if (pokemon === undefined) {
      throw new Error("favorite.service.ts: Pokemon does not exist.");
    }

    if (this.userService.checkIfUserHasPokemon(pokemonName)) {
      throw new Error("favorite.service.ts: User already has this pokemon.");
    }

      // User
     return this.http.patch<Trainer>(`${url}/${userId}`, {
       pokemon: [...this.userService.user.pokemon, pokemonName]
       },
        {headers:
            new HttpHeaders({'Content-type':'application/json','x-api-key':apiKey})}).subscribe(
              (response: Trainer) => {
                this.userService.user = response;
              }
     )}


}
