import { Injectable } from '@angular/core';
import {PokemonService} from "./pokemon.service";
import {UserService} from "./user.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Subscription} from "rxjs";
import {PokemonResult} from "../models/pokemon.model";
import {environment} from "../../environments/environment";
import {Trainer} from "../models/trainer.model";

const url = environment.apiTrainer;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class RemoveService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonService,
    private userService: UserService
  ) { }

  // Removes a pokemon and updates user.
  public removePokemon(pokemonName: string): Subscription {
    if (!this.userService.user) {
      throw new Error("remove.service.ts: User does not exist.");
    }

    const userId: number = this.userService.user.id;
    const pokemon: PokemonResult | undefined = this.pokemonService.pokemon.find(p => p.name === pokemonName);

    if (pokemon === undefined) {
      throw new Error("remove.service.ts: Pokemon does not exist.");
    }

    if (!this.userService.checkIfUserHasPokemon(pokemonName)) {
      throw new Error("remove.service.ts: User does not have this pokemon.");
    }

    return this.http.patch<Trainer>(`${url}/${userId}`, {
      pokemon: [...this.userService.user.pokemon.filter(p => typeof p === "string" && p !== pokemonName)]
    },
      {headers:
          new HttpHeaders({'Content-type':'application/json','x-api-key':apiKey})}).subscribe(
       (response: Trainer) => {
           this.userService.user = response;
}
)}
}
