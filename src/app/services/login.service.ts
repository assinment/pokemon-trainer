import { Injectable } from '@angular/core';
import {map, Observable, of, switchMap} from "rxjs";
import {Trainer} from "../models/trainer.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

const url = environment.apiTrainer;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly http: HttpClient) { }

// If user does not exist it creates a new user.
public login(username: string): Observable<Trainer> {
    return this.checkUser(username)
      .pipe(
        switchMap((response: Trainer | undefined) => {
          if (response === undefined) {
            return this.createUser(username)
          }
            return of(response);
        })
      )
}

  private checkUser(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${url}?username=${username}`)
      .pipe(
        map((response: Trainer[]) => response.pop())
      )
  }

  private createUser(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: []
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "x-api-key": apiKey
    });

    return this.http.post<Trainer>(url, trainer, {headers})
  }
}
