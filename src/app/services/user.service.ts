import { Injectable } from '@angular/core';
import {Trainer} from "../models/trainer.model";
import {StorageUtil} from "../utils/storage.util";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: Trainer;

  public get user(): Trainer | undefined {
    return this._user
  }

  public set user(user: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>("user", user!)
    this._user = user
  }

  public checkIfUserHasPokemon(pokemonName: string): boolean {
    if (this.user) {
      return Boolean(this.user.pokemon.find(
        p => typeof p  === 'string' && p === pokemonName
      ));
    }
    else return false;
  }


  constructor() {
    this._user = StorageUtil.storageRead<Trainer>("user");
  }
}
